const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')

const config = {
    mode: 'production',
    entry: {
        app: './src/index.js',
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        port: 1991,
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'webpack & vue',
            template: './public/index.html',
        }),
        new CleanWebpackPlugin({ dry: true }),
        new VueLoaderPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: ['vue-loader'],
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        // filename: "[name].bundle.js",
        filename: '[name].[chunkhash].js',
        clean: true,
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },
    performance: { hints: false },
}

module.exports = config
